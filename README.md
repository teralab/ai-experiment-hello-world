# AI Experiment Hello World

This repository contains a basic Hello World example, walking you through the basic concepts of building an AI Solutions for AI4Experiments.  
It is written in NodeJS with comments explaining the most important lines of code.  
It implements two different setups highlighting the key concepts and most important things and consider if you want to develop your own models and solutions to be executed within AI4Experiments.

## purpose

The purpose of this hello world is to propose an first basic implementation of two or more containers exchanging very simple text message so people can:
1. understand how a solution is built
2. test basic models and solutions
3. start implementing your own from a working example.

## Context

This asset has been developed in the context of the EU-funded ASSISTANT Project in order to help dissiminate AI models and tools through the AIoD platform.

### The Assistant Project
![](assets/assistant_logo.png){height=125px}

_The EU-funded ASSISTANT project (project no. 101000165) aims to develop breakthrough solutions for the manufacturing industry, using artificial intelligence to optimize production systems. One of the keystones of ASSISTANT is the creation of intelligent digital twins. By combining machine learning, optimization, simulation, and domain models, ASSISTANT develops tools and solutions providing all required information to help production managers design production lines, plan production, and improve machine settings for effective and sustainable decisions that guarantee product quality and safety._

https://assistant-project.eu/  
Funding by the European Union’s research and innovation programme 'H2020'call H2020-ICT-2018-20 under grant agreement no. 101000165.

### The AI-On-Demand (AIoD) Platform

![](https://www.ai4europe.eu/sites/default/files/logo_aiodp_161_80.svg){height=100px}

https://www.ai4europe.eu/

_AI4Experiments is an open source platform for the development, training, sharing and deployment of AI models.The platform offers visual and intuitive design methods. It facilitates the creation of human-centered AI-solutions, building modular structures and using hybrid AI technologies.It implements the AI4EU Container specification and more precisely the usage of gRPC protocol._

The source code is shared through an Eclipse Foundation Project: https://gitlab.eclipse.org/eclipse/graphene  
AI4Experiments is part of the AI-on-Demand Platform (AIoD).


## Before you start

Basic informations and reference to be knowned before testing the code:
* gRPC is based on the Protocol Buffers -> [More about gRPC](https://grpc.io/)
* AIoD allows you to create, maintain and share two different kind of resources:
  * **Models**: AI Models are containers implementing gRPC to deals with Input and Output. Models are packaged as Docker Container. They are the key building blocks to compose solutions.
  * **Solution**: Solutions are composed by a graph of interconnected Models. It allow to build a workflow (or pipeline) providing a more high-level service for a specific use case.
  * [Direct link to AI Experiments](https://aiexp.ai4europe.eu/#/home)
  * [AI4Experiments Platform manual](https://github.com/ai4eu/tutorials/blob/master/Deliverable_AI4EU_D3.3_Platfrom_Manual.pdf)
  * [Extra ressources and tutorials](https://github.com/ai4eu/tutorials)
  

## Tutorial

### Architecture

**Note:** We recommand to start working locally using docker-compose to better understand the architecture and implement your first tests.

As a first step, we will create two services : a gRPC server (SRV) and a gRPC client (CLIENT) each implementing their own web interfaces.  
It is a very simple use case where two components exchange data to communicate.
The client's web interface allows its user to enter his name. After validation, the client invoke the **sayHello** method on SRV.
The purpose is not to have anything complex. The **sayHello** method just reply `"hello <user>".`

The server web interface displays a history of all the messages received and processed since the initialisation of the container.

### Things to know

By convention on AI Experiment:
- From your container, you need to set RPC (mandatory) on port 8061 and the web interface (optional) of your models on port 8062.
- The format of input and output message should be described with a protobuf file. While publishing your model on the AIoD, you will need to upload the protobuf file on the platform so it could be used to construct Pipelines in the designer tool.

We have located the protobuf file in the corresponding repository:
* [server's protobuf](srv/greeter-srv.proto) 

### Deploy locally

To test our models, we can deploy them locally and make them communicate together. Below are the component and sequence diagram of the system:

![FIG 1](assets/compose_dg.svg)

![FIG 2](assets/compose_sq.svg)

1. [Install Docker Compose](https://docs.docker.com/compose/install/) locally

2. Clone this repository

3. To build images and run the containers. From the project directory 

```bash
$ docker-compose up -d --build
```

4. Check that all containers are up and running

```bash
$ docker-compose ps
```

5. When everything is up, the web 
interfaces of the client and srv will be available at respectively : http://localhost:8062 and http://localhost:8063.

The GIF immage below shows how it works in action

![FIG 3](assets/compose_demo.gif)


### Deploy as a AI Experiment Solution  

### Architecture

To avoid modifying a docker-compose file each time the pipeline is modified, the AI Experiment will "package" your solution and add an extra container "Orchestrator" and a script. 

As its name suggests, the orchestrator orchestrates the communication between all the containers/models. We still have the same two models but this time we have to consider the orchestator provided by AIoD as well.

Below are the new component and sequence diagram of the system. You can note the difference from the previous case. This time all the input and output are centralised through the Orchestrator:

![FIG 4](assets/aiod_dg.svg)

![FIG 5](assets/aiod_sq.svg)

The protobuf of SRV stays the same.
However, the client as an AI model, must also have its protobuf file by which it exposes it own services (the proto buff can be found [here](client/greeter-ui.proto)). 

Here are the benefits and changes: 
* A solution can included many different containers/models. The design studio of AI Experiment allow to link Inputs and Outputs according to each protobuff files. The orchestrator polls every input from all the models, and once available, push it through the whole pipeline.
* To receive message, the Client can no longer directly invoke the **sayHello** method on SRV, instead it must provide two methods that will be invoked by the orchestrator when receiving the corresponding data through the pipeline and process the result. Those methods are :
  * **requestGreeting** : this method is the starting point of the pipeline (because of the "Empty" input, refer to the [AIoD Platform manual](https://github.com/ai4eu/tutorials/blob/master/Deliverable_AI4EU_D3.3_Platfrom_Manual.pdf)) and is responsible for generating data that goes into the pipeline. We implement a queue on the Client model. When the user submits his name via the client's web interface, the client adds it in to the queue. When the orchestrator polls for input (and invokes this requestGreeting method), we remove the first item of the queue and send it a response.
  * **processResult** : this is the ending point of the pipeline (because of the "Empty" return) and is responsible taking the response generated by SRV and display it on the client's web interface.

### Deployment

First, to be accessible through the AI4Experiments platform, your docker container needs to be accessible through a docker repository
You need to connect to your container repository, then build the image from your sources and push it to the repository.


```bash
$ docker login -u <mylogin>
$ cd /client
$ docker build -t <container_registry_path>/<project>/ai-experiment-hello-world/client:v1 .
$ docker push <container_registry_path>/<project>/ai-experiment-hello-world/client:v1
$ cd ../srv
$ docker build -t <container_registry_path>/<project>/ai-experiment-hello-world/srv:v1 .
$ docker push <container_registry_path>/<project>/ai-experiment-hello-world/srv:v1
```
Now that your containers are accessible, you can publish them through AI Experiment to build and execute a solution.
It could be done in three parts:
1. Publish the model on AI Experiments
2. Design your solution using your models
3. Execute your solutions in kubernetes

This [demonstration video](assets/demo_aiod_helloworld.webm) comes in complement to the text below.

#### Publish the models on AI Experiments

After login in on AI Experiments, we need to "onboard" two models one for the CLIENT and the other for the SRV. To do so :

* Click on "Onbard Model" in the sidebar
* Fill in the form by specifying: 
  * the name of the model
  * the docker image location made of:
    * the address of the registry ("docker.io" is the asnwer if the image is on dockerhub)
    * the image name (in the previous tag & push example above, that would <mylogin>/client or <mylogin>/srv according to the model)
    * the image tag (in the previous tag & push example above, that would be "v1")
  * the protobuf file (are located in [srv/greeter-srv.proto](srv/greeter-srv.proto) and [client/greeter-ui.proto](client/greeter-ui.proto) for the client and srv respectively)
* Submit and wait for the confirmation.


#### Designing a solution from models

Once you created the two solutions, it is time to piece them together into a solution, for that:
* Click on "Design Studio".
* In the left panel find your two models and drag them into the canvas (remember the schema in the canvas is based on the protobuf file you provided for the model).
* Connect the matching rpc calls, this allows the orhcestrator to establish the redirection flows. 
  * connect **requestGreeting(HelloRequest)** in the client model with **sayHello(HelloRequest)** in the srv model
  * connect **sayHello(HelloReply)** in the srv model to the **processResult(HelloReply)** in the client model
* Save the model (toolbar right above the canvas)
* Validate the model
* Download the model

To test a working example, you can compare the received zip file with the one we placed in this repository [working solution](solution/))


#### Run the solution.

First of all make sure have [kubernetes](kubernetes.io/docs/setup) and Python installed. 

Get inside the solution folder and run :

```bash
$ kubectl create ns hello && python3 kubernetes-client-script.py -n hello
```

Follow the instruction at the end of the script in order to start the orchestrator. Do not forget this step, otherwise the two models won't be able to interact with one another.

## What's Next? Build your own !

Take our container and replace or add the "say_hello" methods with your own according to your usecase.
- It is possible to assemble several client around the server on the same interface (all the message will be gathered)
- You just need to change the type and to rename a method in the protobuf to adjust the behaviour of the container
- with a simple modification, the server user interface can be use as a probe in a pipeline if you need to check inputs or outputs
