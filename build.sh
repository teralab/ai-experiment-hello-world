#!/bin/bash

PREF=docker.io/skante/aiod-helloworld

if [[ "$1" == "" ]] || [[ "$1" == "all" ]] || [[ "$1" == "srv" ]]; then
    docker build -t $PREF:srv-02 srv
    docker push $PREF:srv-02
fi

if [[ "$1" == "" ]] || [[ "$1" == "all" ]] || [[ "$1" == "web" ]]; then
    docker build -t $PREF:ui-02 web
    docker push $PREF:ui-02
fi

