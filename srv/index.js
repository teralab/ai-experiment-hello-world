
if (!process.env.RPC_BIND) {
    require('dotenv').config();
}

const EventEmitter = require('events').EventEmitter, mainEvent = new EventEmitter();

const express = require('express'), 
    app = express(),    
    server = require('http').Server(app),
    io = require('socket.io')(server),
    ProtoServer = require('./utils/protoServer');

var history = [];

// RPC method:
//  * takes a name in parameter
//  * Keeps a history of how many times it was called (n) and when
//  * Retruns 
//    * "Hello ${name}" for the first time
//    * "Hello again ${name} (for the ${n} th time)" for the subsequent times (when name is identical)
// 
function sayHello(call, callback) {
    var name = call.request.name, message = '';
    console.log('gRPC server | Name :', name);
    var n = history.filter(e=>e.name==name).length;
    if (n == 0){
        message = `Hello ${name}`;
    }
    else { 
        var th = (n+1) % 10 == 1 ? 'st' : (n+1) % 10 == 2 ? 'nd' : (n+1) % 10 == 3 ? 'rd' : 'th';
        message = `Hello again ${name} (for the ${(n+1)} ${th} time)`
    }
    
    var h = { date: Date.now(), name, message };
    history.push(h);
    // 
    // Alert UI of new request processed
    // 
    mainEvent.emit('entry', h)
    callback(null, { message });
}



var srv = new ProtoServer(__dirname + '/greeter-srv.proto').addSerivce('Greeter', { sayHello })
srv.listen(process.env.RPC_BIND);

io.on('connection', (socket)=>{
    // 
    // When first rendering, send the whole history to be displayed on the UI
    // 
    socket.emit('history', history);
    
    // 
    // When subsequent entries are available, forward them to be displayed
    // 
    mainEvent.on('entry', (h)=>{
        socket.emit('entry', h);
    });
});


app.set('view engine', 'ejs');

app.use (express.static(__dirname + '/node_modules/socket.io/client-dist'));

app.get('/', (req, res)=>{
    res.render('home');
})

// Start the gRPC server's UI
server.listen(process.env.WEB_PORT, ()=>{
    console.log(`Web UI listening on ${process.env.WEB_PORT}`);
})

