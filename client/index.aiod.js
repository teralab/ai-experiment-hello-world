const EventEmitter = require('events').EventEmitter, mainEvent = new EventEmitter();

const express = require('express'), 
    app = express(),    
    server = require('http').Server(app),
    io = require('socket.io')(server),
    ProtoServer = require('./utils/protoServer');;


var QUEUE = [];
// 
// Whenever the Orchestrator demands entry, fetch the first element of the queue and return it.
// 
var onQueueNotEmpty = (cb, options = {timeout: 1000})=>{
    options = options || {};
    options.cmpt = options.cmpt || 0;

    var item = QUEUE.shift();
    if (item){
        cb(null, item);
    }
    else{
        setTimeout(onQueueNotEmpty, options.timeout || 1000, cb, options )
    }
}

app.set('view engine', 'ejs');

app.use (express.static(__dirname + '/node_modules/socket.io/client-dist'));

app.get('/', (req, res)=>{
    res.render('home');
})


new ProtoServer(__dirname + '/greeter-ui.proto').addSerivce('GreeterUI', {
    requestGreeting : function (call, callback) {
        console.log('UI gRPC requestGreeting | Next in QUEUE : ', QUEUE[0]);
        // 
        // Fetch from the queue and inject the item in the pipeline
        // 
        onQueueNotEmpty((err, name)=>{ 
            callback(null, { name } );
        });
    },

    processResult: function (call, callback) {
        console.log('UI gRPC processResult | HelloReply : ', call.request.message);
        // 
        // Alert the socket upon response received.
        // 
        mainEvent.emit('response', call.request.message)
        callback(null, {});
    },

}).listen(process.env.RPC_BIND);


io.on('connection', (socket)=>{
    // 
    // Greeting requested from UI
    // 
    socket.on('requestGreeting', (name)=>{
        // 
        // Add to queue, next time the orchestrator ticks, it will be removed and injected into the pipeline
        // 
        QUEUE.push(name);
    });
    
    // 
    // Each time we receive a response
    // 
    mainEvent.on('response', (message)=>{
        // 
        // Forward it to the UI
        // 
        socket.emit('processResult', message);
    })
});


module.exports = server;