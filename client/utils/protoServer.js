

var grpc = require('@grpc/grpc-js');
var protoLoader = require('@grpc/proto-loader');


module.exports = class {
    constructor (src){
        this.protoSrc = src;

        this.proto = grpc.loadPackageDefinition(
            protoLoader.loadSync( src, {
                keepCase: true,
                longs: String,
                enums: String,
                defaults: true,
                oneofs: true
            })
        );

        this.server = new grpc.Server();
    }

    addSerivce (s, fns){
        this.server.addService(this.proto[s].service, fns);
        return this;
    }
    listen(bind, msg){
        this.bind = this.bind || bind;

        this.server.bindAsync(bind, grpc.ServerCredentials.createInsecure(), () => {
            this.server.start();
            console.log(msg || `gRPC server started on ${bind}`);
        });
        return this;
    }

    client(service, bind){
        this.bind = this.bind || bind;
        return new this.proto[service](this.bind, grpc.credentials.createInsecure());
    }
}