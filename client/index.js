
if (!(process.env.RPC_BIND && process.env.WEB_PORT)){
    require('dotenv').config();
}

const server = require( `./index.${process.env.CLIENT_SETUP || 'aiod'}` );

// Start the gRPC client's UI
server.listen(process.env.WEB_PORT, ()=>{
    console.log(`Web UI listening on ${process.env.WEB_PORT}`);
})