
const express = require('express'), 
    app = express(),    
    server = require('http').Server(app),
    io = require('socket.io')(server),
    ProtoServer = require('./utils/protoServer');;

let protoClient = new ProtoServer(__dirname + '/greeter-srv.proto').client('Greeter', process.env.RPC_BIND);

app.set('view engine', 'ejs');

app.use (express.static(__dirname + '/node_modules/socket.io/client-dist'));

app.get('/', (req, res)=>{
    res.render('home');
});

io.on('connection', (socket)=>{
    // 
    // Greeting requested from UI
    // 
    socket.on('requestGreeting', (name)=>{
		// 
		// Submit request by invoking the sayHello gRPC method
		// 
        protoClient.sayHello({ name }, function(err, response){
			//
			// Forward the response to the UI
			// 
			socket.emit('processResult', response.message);
		})
    });
});

module.exports = server;
